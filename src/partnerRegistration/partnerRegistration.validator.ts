import { Schema } from 'express-validator';

const partnerRegistrationSchema: Schema = {
  'partner-id': {
    in: ['headers'],
    notEmpty: true
  }
}

export {
  partnerRegistrationSchema
};

import { IRegistrationResponse } from './partnerRegistration.interface';
import partnerCache from '../partner/partner.cache';
import httpClient from './httpClient';

const registerPartner = async (partnerId: string): Promise<IRegistrationResponse> => {
  const partner = await partnerCache.getPartnerById(partnerId);

  const response = await httpClient(partner.features.registration)
    .post('/registration/register', {}, { headers: { 'partner-id': partnerId }});

  return response.data
}

export default { registerPartner };
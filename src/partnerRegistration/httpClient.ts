import axios from 'axios';

const httpClient = (serviceName: string) => axios.create({
  baseURL: `http://localhost:${serviceName}`,
  timeout: 10000
});

export default httpClient;
import { IRegistrationResponse } from './partnerRegistration.interface';
import partnerRegistrationRepository from './partnerRegistration.repository';

const registerPartner = async (partnerId: string): Promise<IRegistrationResponse> => partnerRegistrationRepository.registerPartner(partnerId);

export default { registerPartner };
import { Router, Request, Response } from 'express';
import { checkSchema, validationResult } from 'express-validator';

import partnerRegistrationService from './partnerRegistration.service';
import { partnerRegistrationSchema } from './partnerRegistration.validator';

const partnerRegistrationController = Router();

partnerRegistrationController.post('/register', checkSchema(partnerRegistrationSchema) , async (req: Request, res: Response) => {

  // Validate incoming input
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
      return res.status(400).json({
          errors: errors.array()
      });
  }
  
  const partnerId = req.header('partner-id') as string;

  const response = await partnerRegistrationService.registerPartner(partnerId);

  return res.status(200).json(response)
});

export default partnerRegistrationController;
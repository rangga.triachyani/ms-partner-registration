export interface IPartner {
  id: string;
  features: {
    registration: string
  }
}
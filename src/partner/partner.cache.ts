import redis from '../redis';
import { IPartner } from '../partner/partner.interface';

const getPartnerById = async (partnerId: string): Promise<IPartner> => {
  const partner = await redis.get(partnerId);

  if (!partner) {
    throw new Error()
  }

  return JSON.parse(partner);
}

export default { getPartnerById };
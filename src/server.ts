import express, { Application } from 'express';

import partnerRegistrationController from './partnerRegistration/partnerRegistration.controller';

const app: Application = express();

const port: number = 3000;

app.use('/registration', partnerRegistrationController)

app.listen(port, () => {
    return console.log(`server is listening on ${port}`);
})